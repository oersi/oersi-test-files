# OERSI test files

This repo hosts OER records conforming AMB format for internal testing e.g. new elements.


## Add new tests
1) create a new record as JSON file.
It has to conform the internal AMB application profil of OERSI.

2) copy the gitlab raw data link to this file in a new line into testresource.txt

For testing run the testing workflow on develop.